package com.mrugendra.afterpaycars.data

import com.mrugendra.afterpaycars.BaseUnitTest
import com.mrugendra.afterpaycars.data.models.ApiCar
import com.mrugendra.afterpaycars.domain.CarsRepository
import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.reset
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response
import java.math.BigDecimal
import kotlin.test.assertTrue

@ExperimentalCoroutinesApi
class CarsRepositoryImplTest : BaseUnitTest() {

    private val mockAfterpayApi: AfterpayApi = mock()
    private lateinit var repo: CarsRepository

    override fun setup() {
        reset(mockAfterpayApi)
        repo = CarsRepositoryImpl(mockAfterpayApi, testDispatcher)
    }

    @Test
    fun given_triedFetchingCars_and_successResponse_test_result() {
        runBlockingTest {
            whenever(mockAfterpayApi.getAllCars()).thenReturn(
                Response.success(
                    listOf(
                        ApiCar(
                            make = ApiCar.ApiMake(
                                manufacturer = "honda",
                                model = "civic"
                            ),
                            color = "black",
                            year = "2018",
                            configuration = ApiCar.ApiConfig(
                                body = "sedan",
                                cylinders = null,
                                horsepower = null
                            ),
                            price = BigDecimal(10000),
                            image = null,
                            mpg = null,
                            origin = null
                        )
                    )
                )
            )

            val result = repo.getAllCars()
            assertTrue {
                result is NetworkResult.Success<List<Car>>
                        && result.data.count() == 1
            }
        }
    }

    @Test
    fun given_triedFetchingCars_and_errorResponse_test_result() {
        runBlockingTest {
            whenever(mockAfterpayApi.getAllCars()).thenReturn(
                Response.error(400, ResponseBody.create(null, byteArrayOf()))
            )

            val result = repo.getAllCars()

            assertTrue {
                result == NetworkResult.Error<List<Car>>(400)
            }
        }
    }
}