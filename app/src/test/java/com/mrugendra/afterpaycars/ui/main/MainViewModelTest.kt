package com.mrugendra.afterpaycars.ui.main

import androidx.lifecycle.Observer
import com.mrugendra.afterpaycars.BaseUnitTest
import com.mrugendra.afterpaycars.R
import com.mrugendra.afterpaycars.domain.CarsRepository
import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.Make
import com.mrugendra.afterpaycars.domain.models.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Test
import org.mockito.kotlin.*
import java.math.BigDecimal

@ExperimentalCoroutinesApi
class MainViewModelTest : BaseUnitTest() {

    private val mockCarsRepository: CarsRepository = mock()
    private val mockViewStateObserver: Observer<MainViewModel.ViewState> = mock()

    private lateinit var viewModel: MainViewModel

    override fun setup() {
        super.setup()
        reset(mockCarsRepository, mockViewStateObserver)
        viewModel = MainViewModel(
            carsRepository = mockCarsRepository
        )
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun given_initCalled_and_isSuccessful_testViewState() {
        runBlockingTest {
            whenever(mockCarsRepository.getAllCars()).thenReturn(
                NetworkResult.Success(
                    listOf(
                        Car(
                            make = Make(
                                manufacturer = "honda",
                                model = "civic"
                            ),
                            price = BigDecimal("10000"),
                            year = "2018",
                            imageUrl = "image"
                        )
                    )
                )
            )

            viewModel.viewState.observeForever(mockViewStateObserver)
            viewModel.init(forcedUpdate = false)

            verify(mockViewStateObserver, times(1))
                .onChanged(MainViewModel.ViewState.Loading)
            verify(mockViewStateObserver, times(1))
                .onChanged(
                    MainViewModel.ViewState.CarsData(
                        data = listOf(
                            MainViewModel.ViewState.CarsData.CarItem(
                                title = "2018 honda civic",
                                subtitle = "$10000",
                                image = "image"
                            )
                        )
                    )
                )
        }
    }

    @Test
    fun given_initCalled_and_isFailure_testViewState() {
        runBlockingTest {
            whenever(mockCarsRepository.getAllCars()).thenReturn(
                NetworkResult.Error(0)
            )

            viewModel.viewState.observeForever(mockViewStateObserver)
            viewModel.init(forcedUpdate = false)

            verify(mockViewStateObserver, times(1))
                .onChanged(MainViewModel.ViewState.Loading)
            verify(mockViewStateObserver, times(1))
                .onChanged(
                    argThat<MainViewModel.ViewState.Error> {
                        this.messageRes == R.string.cars_loading_failed_message
                    }
                )
        }
    }

    @Test
    fun given_initCalled_and_isSuccessWithEmptyResponse_testViewState() {
        runBlockingTest {
            whenever(mockCarsRepository.getAllCars()).thenReturn(
                NetworkResult.Success(emptyList())
            )

            viewModel.viewState.observeForever(mockViewStateObserver)
            viewModel.init(forcedUpdate = false)

            verify(mockViewStateObserver, times(1))
                .onChanged(MainViewModel.ViewState.Loading)
            verify(mockViewStateObserver, times(1))
                .onChanged(
                    MainViewModel.ViewState.NoData(
                        messageRes = R.string.no_cars_available_message
                    )
                )
        }
    }
}