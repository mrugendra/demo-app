package com.mrugendra.afterpaycars.ui.main

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mrugendra.afterpaycars.R
import com.mrugendra.afterpaycars.domain.CarsRepository
import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val carsRepository: CarsRepository
) : ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState

    fun init(forcedUpdate: Boolean = false) {
        if (forcedUpdate || _viewState.value == null) {
            viewModelScope.launch {
                _viewState.postValue(ViewState.Loading)
                val viewState = when (val result = carsRepository.getAllCars()) {
                    is NetworkResult.Success -> {
                        if (result.data.isEmpty()) {
                            ViewState.NoData(messageRes = R.string.no_cars_available_message)
                        } else {
                            ViewState.CarsData(result.data.map { it.toUiModel() })
                        }
                    }
                    else -> ViewState.Error(messageRes = R.string.cars_loading_failed_message) {
                        init(forcedUpdate = true)
                    }
                }
                _viewState.postValue(viewState)
            }
        }
    }

    private fun Car.toUiModel(): ViewState.CarsData.CarItem {
        return ViewState.CarsData.CarItem(
            image = imageUrl,
            title = "$year ${make.manufacturer} ${make.model}",
            subtitle = "$$price"
        )
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class CarsData(val data: List<CarItem>) : ViewState() {
            data class CarItem(val image: String?, val title: String, val subtitle: String)
        }

        data class NoData(@StringRes val messageRes: Int) : ViewState()
        data class Error(@StringRes val messageRes: Int, val onRetry: () -> Unit) : ViewState()
    }
}