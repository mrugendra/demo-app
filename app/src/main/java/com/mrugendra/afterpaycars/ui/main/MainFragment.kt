package com.mrugendra.afterpaycars.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.mrugendra.afterpaycars.R
import com.mrugendra.afterpaycars.databinding.MainFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private val viewBindings: MainFragmentBinding by lazy {
        MainFragmentBinding.inflate(LayoutInflater.from(requireContext()))
    }
    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = viewBindings.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBindings.apply {
            carList.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = CarsAdapter()
            }
        }
        viewModel.apply {
            viewState.observe(viewLifecycleOwner, ::renderViewState)
            init()
        }
    }

    private fun renderViewState(viewState: MainViewModel.ViewState) {
        viewBindings.apply {
            viewProgress.gone()
            when (viewState) {
                is MainViewModel.ViewState.Loading -> {
                    viewProgress.visible()
                    viewError.gone()
                }
                is MainViewModel.ViewState.CarsData -> {
                    viewError.gone()
                    carList.apply {
                        visible()
                        (adapter as CarsAdapter).data = viewState.data
                    }
                }
                is MainViewModel.ViewState.NoData -> {
                    viewError.apply {
                        visible()
                        tvMessage.setText(viewState.messageRes)
                        btnRetry.gone()
                    }
                    carList.gone()
                }
                is MainViewModel.ViewState.Error -> {
                    viewError.apply {
                        visible()
                        tvMessage.setText(viewState.messageRes)
                        btnRetry.visible()
                        btnRetry.setOnClickListener { viewState.onRetry() }
                    }
                    carList.gone()
                }
            }
        }
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}