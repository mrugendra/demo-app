package com.mrugendra.afterpaycars.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.mrugendra.afterpaycars.R
import com.mrugendra.afterpaycars.databinding.ItemCarBinding
import com.mrugendra.afterpaycars.domain.models.Car
import kotlin.properties.Delegates

class CarsAdapter : RecyclerView.Adapter<CarsViewHolder>() {

    var data: List<MainViewModel.ViewState.CarsData.CarItem> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarsViewHolder {
        return CarsViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: CarsViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size
}

class CarsViewHolder(private val bindings: ItemCarBinding) :
    RecyclerView.ViewHolder(bindings.root) {


    fun bind(car: MainViewModel.ViewState.CarsData.CarItem) {
        bindings.apply {
            Glide.with(ivThumbnail)
                .load(car.image)
                .placeholder(R.drawable.ic_car_placeholder)
                .error(R.drawable.ic_car_placeholder)
                .transform(
                    CenterCrop(),
                    RoundedCorners(root.context.resources.getDimensionPixelSize(R.dimen.margin_tiny))
                )
                .into(ivThumbnail)

            tvTitle.text = car.title
            tvSubtitle.text = car.subtitle
        }
    }

    companion object {
        fun create(parent: ViewGroup): CarsViewHolder {
            return CarsViewHolder(
                ItemCarBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }
}