package com.mrugendra.afterpaycars.injection

import com.mrugendra.afterpaycars.data.CarsRepositoryImpl
import com.mrugendra.afterpaycars.domain.CarsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideCarsRepository(impl: CarsRepositoryImpl): CarsRepository = impl

    @IODispatcher
    @Provides
    fun providesIODispatcher(): CoroutineDispatcher = Dispatchers.IO
}