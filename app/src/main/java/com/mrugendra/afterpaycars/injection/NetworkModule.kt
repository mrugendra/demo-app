package com.mrugendra.afterpaycars.injection

import android.app.Application
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.mrugendra.afterpaycars.BuildConfig
import com.mrugendra.afterpaycars.data.AfterpayApi
import com.mrugendra.afterpaycars.data.AfterpayApiEndpoints
import com.mrugendra.afterpaycars.data.moshiConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideAfterpayApi(retrofit: Retrofit): AfterpayApi =
        retrofit.create(AfterpayApi::class.java)

    @Provides
    @Singleton
    fun provideOkHttp(
        application: Application
    ): OkHttpClient {
        return OkHttpClient().newBuilder()
            .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(ChuckerInterceptor.Builder(context = application).build())
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            }).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(AfterpayApiEndpoints.BASE_URL)
            .addConverterFactory(moshiConfig)
            .build()
    }

    private const val NETWORK_TIMEOUT: Long = 60

}