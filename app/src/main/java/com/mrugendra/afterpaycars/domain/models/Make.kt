package com.mrugendra.afterpaycars.domain.models

data class Make(val manufacturer: String, val model: String)
