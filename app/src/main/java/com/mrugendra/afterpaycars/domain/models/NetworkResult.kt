package com.mrugendra.afterpaycars.domain.models

sealed class NetworkResult<T> {
    data class Success<T>(val data: T) : NetworkResult<T>()
    data class Error<T>(val errorCode: Int) : NetworkResult<T>()
}
