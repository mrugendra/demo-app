package com.mrugendra.afterpaycars.domain

import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.NetworkResult

interface CarsRepository {
    suspend fun getAllCars(): NetworkResult<List<Car>>
}