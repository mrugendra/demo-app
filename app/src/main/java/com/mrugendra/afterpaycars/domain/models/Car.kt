package com.mrugendra.afterpaycars.domain.models

import java.math.BigDecimal

data class Car(val make: Make, val year: String, val imageUrl: String?, val price: BigDecimal)