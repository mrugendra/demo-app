package com.mrugendra.afterpaycars.data.models

import com.squareup.moshi.Json
import java.math.BigDecimal

data class ApiCar(
    @Json(name = "make") val make: ApiMake,
    @Json(name = "color") val color: String,
    @Json(name = "year") val year: String,
    @Json(name = "configuration") val configuration: ApiConfig,
    @Json(name = "origin") val origin: String?,
    @Json(name = "mpg") val mpg: Int?,
    @Json(name = "price") val price: BigDecimal,
    @Json(name = "image") val image: String?
) {
    data class ApiMake(
        @Json(name = "manufacturer") val manufacturer: String,
        @Json(name = "model") val model: String,
    )

    data class ApiConfig(
        @Json(name = "body") val body: String,
        @Json(name = "cylinders") val cylinders: Int?,
        @Json(name = "horsepower") val horsepower: Int?,
    )
}