package com.mrugendra.afterpaycars.data

import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.math.BigDecimal

val moshiConfig = MoshiConverterFactory.create(
    Moshi.Builder()
        .add(BigDecimalAdapter)
        .addLast(KotlinJsonAdapterFactory())
        .build()
)

private object BigDecimalAdapter {
    @FromJson
    fun fromJson(string: String) = BigDecimal(string)

    @ToJson
    fun toJson(value: BigDecimal) = value.toString()
}
