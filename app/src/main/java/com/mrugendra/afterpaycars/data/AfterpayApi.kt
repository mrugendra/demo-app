package com.mrugendra.afterpaycars.data

import com.mrugendra.afterpaycars.data.models.ApiCar
import retrofit2.Response
import retrofit2.http.GET

interface AfterpayApi {

    @GET(AfterpayApiEndpoints.BASE_URL + AfterpayApiEndpoints.PATH_CARS)
    suspend fun getAllCars(): Response<List<ApiCar>>

}