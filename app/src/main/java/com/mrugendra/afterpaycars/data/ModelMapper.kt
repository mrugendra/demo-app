package com.mrugendra.afterpaycars.data

import com.mrugendra.afterpaycars.data.models.ApiCar
import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.Make
import com.mrugendra.afterpaycars.domain.models.NetworkResult
import retrofit2.Response

/**
 * TODO: Write unit tests for all mappers
 */

fun ApiCar.toDomainModel(): Car {
    return Car(
        make = make.toDomainModel(),
        price = price,
        year = year,
        imageUrl = (AfterpayApiEndpoints.BASE_URL + image).takeIf { !this.image.isNullOrBlank() }
    )
}

fun ApiCar.ApiMake.toDomainModel(): Make {
    return Make(
        manufacturer = manufacturer,
        model = model
    )
}

fun <T, K> Response<T>.toResult(processor: (T.() -> K)? = null): NetworkResult<K> {
    return if (this.isSuccessful) {
        NetworkResult.Success(
            processor?.invoke(requireNotNull(body())) ?: requireNotNull(body()) as K
        )
    } else {
        NetworkResult.Error(this.code())
    }
}
