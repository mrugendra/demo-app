package com.mrugendra.afterpaycars.data

object AfterpayApiEndpoints {
    const val BASE_URL = "https://afterpay-mobile-interview.s3.amazonaws.com/"

    const val PATH_CARS = "cars.json"

    const val PATH_CARS_EMPTY = "cars_empty.json"
    const val PATH_CARS_ERROR = "cars_error.json"

}