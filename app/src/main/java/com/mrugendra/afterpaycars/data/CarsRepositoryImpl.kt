package com.mrugendra.afterpaycars.data

import com.mrugendra.afterpaycars.domain.CarsRepository
import com.mrugendra.afterpaycars.domain.models.Car
import com.mrugendra.afterpaycars.domain.models.NetworkResult
import com.mrugendra.afterpaycars.injection.IODispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class CarsRepositoryImpl @Inject constructor(
    private val afterpayApi: AfterpayApi,
    @IODispatcher private val dispatcher: CoroutineDispatcher
) : CarsRepository {

    override suspend fun getAllCars(): NetworkResult<List<Car>> {
        return withContext(dispatcher) {
            try {
                afterpayApi.getAllCars().toResult { this.map { it.toDomainModel() } }
            } catch (e: Exception) {
                Timber.e(e, "Error fetching cars")
                NetworkResult.Error(404)
            }
        }
    }

}