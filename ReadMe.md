- Focused Areas 
    - Architecture: App follows clean architecture, which helps with separation of
  concerns, creates good level of abstractions between layers(UI, Domain, & Data) allowing only
  visibility of layer below it (UI -> Domain -> Data). This also allows possibility of doing changes
  in specific layer (e.g. data, if we change the api), while keeping changes in rest of the app to
  minimum.

    - Testing: App is tested with Unit tests. Considering this is very basic app, it can do with very
little to no UI tests. We can mostly rely on Unit tests. As ViewModels emit specific ViewState
instances to change UI, UI can be tested using FragmentScenario and ActivityScenario to keep it
simple and stable instead of flaky and hard to maintain Espresso Tests.
      
      

